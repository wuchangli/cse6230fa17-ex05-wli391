const char help[] = "Test driver for the correctness of matrix-matrix multiply-add implementation";

#include <petscviewer.h>
#include <petscblaslapack.h>
#include "mmma.h"

int main(int argc, char **argv)
{
  PetscInt       test, numTests = 10;
  PetscInt       scale = 20;
  PetscRandom    rand;
  MPI_Comm       comm;
  PetscViewer    viewer;
  PetscErrorCode ierr;

  ierr = PetscInitialize(&argc, &argv, NULL, help); if (ierr) return ierr;
  comm = PETSC_COMM_WORLD;
  viewer = PETSC_VIEWER_STDOUT_WORLD;
  ierr = PetscOptionsBegin(comm, NULL, "Matrix-Matrix Multiply-Add Test Options", "test_mmma.c");CHKERRQ(ierr);
  ierr = PetscOptionsInt("-num_tests", "Number of tests to run", "test_mmma.c", numTests, &numTests, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsInt("-scale", "Scale (log2) of the matrices in the test", "test_mmma.c", scale, &scale, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsEnd();CHKERRQ(ierr);

  ierr = PetscRandomCreate(comm, &rand);CHKERRQ(ierr);
  ierr = PetscRandomSetFromOptions(rand);CHKERRQ(ierr);
  ierr = PetscRandomSeed(rand);CHKERRQ(ierr);

  ierr = PetscViewerASCIIPrintf(viewer, "Running %D tests of matrix_matrix_multiply_add()\n", numTests);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPushTab(PETSC_VIEWER_STDOUT_WORLD);
  for (test = 0; test < numTests; test++) {
    PetscScalar *Aa, *Ba, *Ca, *Da, *Ea;
    PetscReal    logm, logn, logr, alpha, margin;
    PetscReal    maxThresh;
    PetscInt     m, n, r, i;

    ierr = PetscViewerASCIIPrintf(viewer, "Test %D:\n", test);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPushTab(PETSC_VIEWER_STDOUT_WORLD);
    ierr = PetscRandomSetInterval(rand, 0, (PetscReal) scale);CHKERRQ(ierr);
    ierr = PetscRandomGetValueReal(rand, &logm);CHKERRQ(ierr);
    logn = scale - logm;

    ierr = PetscRandomSetInterval(rand, 0, PetscMin(logn, logm));CHKERRQ(ierr);
    ierr = PetscRandomGetValueReal(rand, &logr);CHKERRQ(ierr);

    m = (PetscInt) PetscPowReal(2., logm);
    n = (PetscInt) PetscPowReal(2., logn);
    r = (PetscInt) PetscPowReal(2., logr);

    ierr = PetscViewerASCIIPrintf(viewer, "Test dimensions: C = [%D x %D], A = [%D x %D], B = [%D x %D]\n", m, n, m, r, r, n);CHKERRQ(ierr);

    ierr = PetscMalloc5(m*n, &Ca, m*r, &Aa, r*n, &Ba, m*n, &Da, m*n, &Ea);CHKERRQ(ierr);

    ierr = PetscRandomSetInterval(rand, -1., 1.);CHKERRQ(ierr);

    ierr = PetscRandomGetValue(rand, &alpha);CHKERRQ(ierr);

    for (i = 0; i < m * n; i++) {
      ierr = PetscRandomGetValue(rand, &Ca[i]);CHKERRQ(ierr);
      Da[i] = Ca[i];
      Ea[i] = PetscAbsReal(Da[i]);
    }
    for (i = 0; i < m * r; i++) {
      ierr = PetscRandomGetValue(rand, &Aa[i]);CHKERRQ(ierr);
    }
    for (i = 0; i < r * n; i++) {
      ierr = PetscRandomGetValue(rand, &Ba[i]);CHKERRQ(ierr);
    }

#if defined(PETSC_USE_REAL_DOUBLE)
    ierr = matrix_matrix_multiply_add_double((size_t) m, (size_t) n, (size_t) r, alpha, Ca, Aa, Ba);CHKERRQ(ierr);
#elif defined(PETSC_USE_REAL_SINGLE)
    ierr = matrix_matrix_multiply_add_single((size_t) m, (size_t) n, (size_t) r, alpha, Ca, Aa, Ba);CHKERRQ(ierr);
#else
    SETERRQ(comm,PETSC_ERR_LIB,"Precision not supported by test");
#endif


    {
      PetscBLASInt mb = m, nb = n, rb = r;
      PetscReal one = 1.;

      PetscStackCallBLAS("BLASgemm", BLASgemm_("N", "N", &nb, &mb, &rb, &one, Ba, &nb, Aa, &rb, &alpha, Da, &nb));
    }

    for (i = 0; i < m * r; i++) {
      Aa[i] = PetscAbsReal(Aa[i]);
    }
    for (i = 0; i < r * n; i++) {
      Ba[i] = PetscAbsReal(Ba[i]);
    }

    alpha = PetscAbsReal(alpha);

    {
      PetscBLASInt mb = m, nb = n, rb = r;
      PetscReal one = 1.;

      PetscStackCallBLAS("BLASgemm", BLASgemm_("N", "N", &nb, &mb, &rb, &one, Ba, &nb, Aa, &rb, &alpha, Ea, &nb));
    }

    margin = PetscPowReal(((PetscReal) 1. + (PetscReal) PETSC_MACHINE_EPSILON),2. * (2. * r + 1)) - 1.;
    maxThresh = 0.;
    for (i = 0; i < m *  n; i++) {
      PetscReal res = Da[i] - Ca[i];
      PetscReal thresh = margin * Ea[i];

      maxThresh = PetscMax(thresh,maxThresh);
      if (PetscAbsReal(res) > thresh) {
        SETERRQ5(comm, PETSC_ERR_LIB, "Test %D failed error test at (%D,%D) threshold %g with value %g\n", test, i / n, i % n, (double) thresh, (double) res);
      }
    }
    ierr = PetscFree5(Ca, Aa, Ba, Da, Ea);CHKERRQ(ierr);

    ierr = PetscViewerASCIIPushTab(PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPrintf(viewer, "Maximum threshold: %g\n", (double) maxThresh);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPrintf(viewer, "Passed.\n");CHKERRQ(ierr);

    ierr = PetscViewerASCIIPopTab(PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPopTab(PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  }
  ierr = PetscViewerASCIIPopTab(PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  ierr = PetscRandomDestroy(&rand);CHKERRQ(ierr);

  ierr = PetscFinalize();
  return ierr;
}
