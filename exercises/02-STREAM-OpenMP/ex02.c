#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <math.h>
#include <tictoc.h>

#if !defined(N)
#define N 4000000
#endif

#if !defined(NTIMES)
#define NTIMES 10
#endif

#define myaligned __attribute__ ((aligned (__BIGGEST_ALIGNMENT__)));

static double A[N] myaligned;
static double B[N] myaligned;
static double C[N] myaligned;

void dummy(double *,double *,double *);

int main(int argc, char **argv)
{
  int    i;
  double min_time = DBL_MAX, max_time = 0.0, avg_time = 0.0;
  double aval = 1., bval = 2., cval = 3., s = 4.;
  double rate;

  if (argc > 1) aval = atof(argv[1]);
  if (argc > 2) bval = atof(argv[2]);
  if (argc > 3) cval = atof(argv[3]);
  if (argc > 4) s    = atof(argv[4]);

  #pragma omp parallel for
  for (i = 0; i < N; i++) {
    A[i] = aval;
    B[i] = bval;
    C[i] = cval;
  }

  for (i = 0; i < NTIMES; i++) {
    int         j;
    TicTocTimer timer;
    double      time;

    /* call an externally defined function so that the compiler cannot
     * precompute the results of the loop */
    dummy(A,B,C);

    timer = tic();
    #pragma omp parallel for
    for (j = 0; j < N; j++) {
      A[j] = B[j] + s*C[j];
    }
    time = toc(&timer);

    if (i > 0) {
      min_time = time < min_time ? time : min_time;
      max_time = time > max_time ? time : max_time;
      avg_time += time;
    }

  }

  avg_time /= NTIMES - 1;

  rate = N * 3. * sizeof(double) / avg_time / 1.e9;

  printf("Triad bandwidth %g GB/s (min time %g s, max time %g s, avg time %g s)\n",rate,min_time,max_time,avg_time);

  return 0;
}
