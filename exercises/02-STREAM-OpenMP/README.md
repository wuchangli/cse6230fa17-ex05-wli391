# Exercise 2: STREAM & OpenMP

* Due: Monday, August 28, 2017 [Anywhere on Earth](https://www.timeanddate.com/time/zones/aoe)

* Please submit your exercise to T-Square.

---

* 1) Implement a benchmark like STREAM triad:

~~~
N = 4000000 // A number that should big enouch that no vector fits in any cache
// A, B, C are vectors of size N

A[:] = 1
B[:] = 2
C[:] = 3
s = 4
for j=1:NTIMES
  // Aggregate bandwidth statistics (max, min, avg) for this loop
  A[:] = B[:] + s * C[:]
~~~

  (See [notes/openMP/openmp-ex15.c](../../notes/openMP/openmp-ex15.c) for an
  example of how to aggregate statistics)

  You are welcome to look at the `stream.c` source, but it would probably be
  just as fast to start from scratch.

* 2) Use openMP to parallelize your benchmark.

* 3) Submit: a listing of your code, your compiler optimizations, and a plot of
  the bandwidth as a function of the number of OpenMP threads.  Include on your
  plot a line representing the peak theoretical bandwidth from main memory from
  [exercise 01](../01-Introduction-STREAM/README.md)

* 4) (Optional) Compile your code again, but without optimizations (e.g. `gcc
  -O0`) and plot the results.  My guess is you will see worse absolute
  performance for each thread count.  But, if you *normalize* each curve to the
  serial performance (divide by the bandwidth for `OMP_NUM_THREADS=1`), you
  will now have a plots comparing the speedup of the two programs.  Which shows
  better scalability?
